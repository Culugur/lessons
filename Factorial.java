package rabota;

import java.util.Scanner;
/* Написать программу высчитывающую факториал введенного целого числа
* */

public class Factorial {
    public static void main(String[] args) {// вход в программу
        Scanner s = new Scanner(System.in);
        System.out.print("Введите строку: ");
        int test = s.nextInt();
        int t = factorial(test); // Вызывается функция factorial
        System.out.println(t);

        int t1 = factorialWhile(test);
        System.out.println(t1);
    }

    public static int factorial(int test) {
        int t = 1;
        for (int i = 1; i <= test; i++) {
            t *= i;
        }
        return t;
    }

    public static int factorialWhile(int test) {
        int t = 1;
        int i = 1;
        while (i <= test) {
            t *= i;
            i++;
        }
        return t;
    }
}
