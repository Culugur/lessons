package rabota;

import java.util.Scanner;

public class SummaCifr {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Введите строку: ");
        int test = s.nextInt();
        int cn = summa(test);
        System.out.println(cn);
    }

    public static int summa(int test) {
        int cn = 0;
        for (int j = test; j != 0; j /= 10) {
            cn += j % 10;
        }
        return cn;
    }
}

