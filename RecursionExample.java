package rabota;

/* Написать программу высчитывающую факториал введенного целого числа
 * */
public class RecursionExample {// факториал
//   1 * 2 * 3 * 4 * 5 * 6 = 6!
    public static void main(String[] args) {
        int factori = factorial(6);
        System.out.println(factori);
    }

    public static int factorial(int value) {
        if (value == 1) {
            return 1;
        }
        //  6 * (5 * (4 * (3 * 1))))
        return value * factorial(value - 1);
    }

}
